import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, AlertController, LoadingController, MenuController } from 'ionic-angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { Storage } from '@ionic/storage';
import { User, Api } from '../../providers';
import { MainPage } from '../';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  viewLogin: boolean = true;
  divLogin: boolean = true;
  divMudarSenha: boolean = true;

  rowFingerprint: boolean = false;
  useFingerprint: boolean = false;

  email: string;
  codigo: string;
  senha: string;
  confirma: string;

  account: { username: string, password: string } = {
    username: '',
    password: ''
  };

  newAccount: { cadastro_ancp: number, username: string, password: string, email: string, confirm: string } = {
    cadastro_ancp: 1,
    username: '',
    password: '',
    email: '',
    confirm: ''
  };

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public alertCtrl: AlertController,
    public api: Api,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    private menu: MenuController,
    private faio: FingerprintAIO) {

    this.menu.swipeEnable(false);
    this.verifyUserSaved();
      //DL descomentou de if ate this storage em 8 de agosto de 2020
     if (this.faio.isAvailable) {
       this.rowFingerprint = true;
       this.storage.get('useFingerprint')
         .then((val) => {
           this.useFingerprint = val;
         });
     }

     this.storage.get('token').then(resp => {
      if (resp) {
        if (this.faio.isAvailable && this.useFingerprint) {
          this.doLoginWithFingerprint();
        } else {
          this.navCtrl.push(MainPage);
        }
      }
    });

  }

  /*verifyUserSaved() {
    var _usuarioSalvo = JSON.parse(localStorage.getItem('usuarioSalvo'));
    if (_usuarioSalvo != null && _usuarioSalvo != undefined) {
      let alert = this.alertCtrl.create({
        title: 'Deseja obter usuario salvo?',
        message: 'O app reconhece seu ultimo usuário salvo, deseja logar com ele?',
        buttons: [
          {
            text: 'Não'
          },
          {
            text: 'Sim',
            handler: () => {
              this.account = _usuarioSalvo;
            }
          }
        ]
      });
      alert.present();
    }
  }*/


  verifyUserSaved() {
    var _usuarioSalvo = JSON.parse(localStorage.getItem('usuarioSalvo'));
    this.account = _usuarioSalvo;
  }

  doLogin() {
    if (this.account.password == '' || this.account.username == '') {
      let toast = this.toastCtrl.create({
        message: 'Todos campos são obrigatórios',
        duration: 3000,
        position: 'down'
      });
      toast.present();
    } else {
      let loading = this.loadingCtrl.create({
        content: 'Efetuando login...'
      });
      loading.present();
      this.api.post("vegan-token3", this.account).subscribe((resp: any) => {
        loading.dismiss();
        if (!resp['user']) {
          let toast = this.toastCtrl.create({
            message: resp.toString(),
            duration: 3000,
            position: 'down'
          });
          toast.present();

        } else {
          this.storage.set('dados', resp);
          this.storage.set('logado', true);
          this.storage.set('token', resp.token);
          this.storage.set('cadastroAncp', (resp.user.cadastro_ancp == 1) ? true : false); //DL setei cadastro_ancp==0 era 1 antes
          this.storage.set('username', resp.user.username);
          this.storage.set('animaisSalvos', []);
          this.storage.set('infoAval', "");
          this.showConditions();
          this.navCtrl.push(MainPage);

          if (this.faio.isAvailable) { //DL inseri daqui ate this.show em 8 de agosto de 2020
            this.storage.set('useFingerprint', this.useFingerprint);
          }

         // this.showMessageSaveUser(); //DL comentei 10 de agosto de 2020
        }
      }, (err) => {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: err.error.text,
          duration: 3000,
          position: 'down'
        });
        toast.present();
      });
    }

  }

  /*showMessageSaveUser() {
    let alert = this.alertCtrl.create({
      title: 'Deseja salvar usuário?',
      message: 'Você pode salvar o usuário para logar automaticamente nas outras vezes',
      buttons: [
        {
          text: 'Não Salvar',
          handler: () => {
            localStorage.removeItem('usuarioSalvo');
            this.navCtrl.push(MainPage);
          }
        },
        {
          text: 'Salvar',
          handler: () => {
            localStorage.setItem('usuarioSalvo', JSON.stringify(this.account));
            this.navCtrl.push(MainPage);
          }
        }
      ]
    });
    alert.present();
  }*/

  showMessageSaveUser() {
    localStorage.setItem('usuarioSalvo', JSON.stringify(this.account));
  }

  doLoginWithFingerprint() {
    if (this.faio.isAvailable && this.useFingerprint) {
      this.faio.show({
        clientId: 'ANCP',
        clientSecret: 'password',
        //disableBackup: true, //Only for Android(optional)
        //localizedFallbackTitle: 'Use Pin', //Only for iOS
        //localizedReason: 'Please authenticate' //Only for iOS
      })
        /*.then(result => {
          if (result == "Success") {
            this.navCtrl.push(MainPage);
          } else {
            let toast = this.toastCtrl.create({
              message: result,
              duration: 3000,
              position: 'down'
            });
            toast.present();
          }
        })*/
        .then(result => {
          this.navCtrl.push(MainPage);
        })
        .catch(err => {
          let toast = this.toastCtrl.create({
            message: err.error.text,
            duration: 3000,
            position: 'down'
          });
          toast.present();
        });
    }
  }

  get useFingerprintChange() {
    return this.useFingerprint;
  }

  set useFingerprintChange(value) {
    this.useFingerprint = value;
  }

  doSubscribe() {
    if (this.newAccount.confirm == '' || this.newAccount.email == '' || this.newAccount.password == '' || this.newAccount.username == '') {
      let toast = this.toastCtrl.create({
        message: 'Todos campos são obrigatórios',
        duration: 3000,
        position: 'down'
      });
      toast.present();
    } else {
      let loading = this.loadingCtrl.create({
        content: 'Efetuando cadastro...'
      });
      loading.present();
      this.newAccount.username = this.newAccount.email;
      this.api.post("signup", this.newAccount).subscribe((resp: any) => {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Seu cadastro foi efetuado com sucesso! Pode logar com usuario e senha que você se registrou',
          duration: 3000,
          position: 'down'
        });
        toast.present();
        this.changeView();
      }, (err) => {
        loading.dismiss();
        if (err.error.text == 'ok') {
          let toast = this.toastCtrl.create({
            message: 'Seu cadastro foi efetuado com sucesso! Pode logar com usuario e senha que você se registrou',
            duration: 3000,
            position: 'down'
          });
          toast.present();
          this.changeView();
        } else {
          let toast = this.toastCtrl.create({
            message: 'Erro ao efetuar cadastro. Tente novamente',
            duration: 3000,
            position: 'down'
          });
          toast.present();
        }

      })
    }
  }

  changeView() {
    this.viewLogin = !this.viewLogin;
  }

  openModalRememberPassword() {
    let alert = this.alertCtrl.create({
      title: 'Informe seu e-mail',
      message: 'Por favor informe seu e-mail de cadastro para receber sua senha',
      inputs: [
        {
          name: 'email',
          placeholder: 'Seu e-mail'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {

          }
        },
        {
          text: 'Enviar',
          handler: data => {
            this.rememberPassword(data);
          }
        }
      ]
    });
    alert.present();
  }

  rememberPassword(email) {
    let loading = this.loadingCtrl.create({
      content: 'Realizando lembrete de senha'
    });
    loading.present();


    this.api.get('lembrar-senha/' + email.email, null).subscribe((resp: any) => {
      let alert = this.alertCtrl.create({
        title: 'Atenção',
        message: resp.mensagem,
      })
      alert.present();
      loading.dismiss();
      if (resp.status) {
        this.email = email.email;
        this.divLogin = false;
        this.divMudarSenha = false;
      }
    }, erro => {
      let alert = this.alertCtrl.create({
        title: 'Atenção',
        message: erro,
      })
      alert.present();
      loading.dismiss();
    });

  }

  validarCodigo() {
    let loading = this.loadingCtrl.create({
      content: 'Validando código...'
    });
    loading.present();
    // API para validar código

    this.api.get('validar-codigo/' + this.email + '/' + this.codigo, null).subscribe((resp: any) => {
      let alert = this.alertCtrl.create({
        title: 'Atenção',
        message: resp.mensagem,
      })
      alert.present();
      loading.dismiss();
      if (resp.status) {
        this.divLogin = false;
        this.divMudarSenha = true;
      }
    }, erro => {
      let alert = this.alertCtrl.create({
        title: 'Atenção',
        message: erro,
      })
      alert.present();
      loading.dismiss();
    });

  }

  trocarSenha() {
    let loading = this.loadingCtrl.create({
      content: 'Alterando sua senha'
    });
    loading.present();
    // API para validar código
    var objeto = {
      senha: this.senha,
      confirma: this.confirma,
      email: this.email
    }
    this.api.post('atualizar-senha', objeto).subscribe((resp: any) => {
      if (resp.status) {
        this.divLogin = true;
        this.divMudarSenha = true;
        resp.mensagem = 'Pronto! Sua senha foi alterada! Pode acessar..';
      }
      let alert = this.alertCtrl.create({
        title: 'Atenção',
        message: resp.mensagem,
      })
      alert.present();
      loading.dismiss();

    }, erro => {
      let alert = this.alertCtrl.create({
        title: 'Atenção',
        message: erro,
      })
      alert.present();
      loading.dismiss();
    });

  }

  showConditions() {
    let alert = this.alertCtrl.create({
      title: 'Termos e Condições',
      message: 'É preciso estar logado para ver os detalhes dos produtos. O EhVeg NÃO edita lista de terceiros, APENAS mostra e faz a devida referência ao proprietário. Se o usuário optar por adicionar ou editar informações, a lista de terceiros NÃO é alterada, e se mantém intacta. A alteração se da na própria lista do EhVeg',
    })
    alert.present();
  }
}
