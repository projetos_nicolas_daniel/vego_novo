import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
import { ConsultaLista, Veganator, ReceitasSalvas, FirstRunPage, CadastroLista, CadastroProduto, ListaProdutoVego, ListaProdutoVegoNovos, ListaProdutoVegoAlta, Patrocinadores, CadastroApoiadores } from '..';
import { Storage } from '@ionic/storage';
import { Api } from '../../providers';
// import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'content.html'
})
export class ContentPage {

  userName: string;
  token: string;

  constructor(public navCtrl: NavController, public storage: Storage, public alertCtrl: AlertController,public api: Api) {
    this.getUser();
    this.getToken();
  }

  openConsultaRapidaDeTouros() {
    this.navCtrl.push(ConsultaLista);
  };

  openPatrocinadores(){
    this.navCtrl.push(Patrocinadores);
  }

  openCadastroApoiadores(){
    this.navCtrl.push(CadastroApoiadores);
  }

  openNovaLista() {
    this.navCtrl.push(CadastroLista);
  };

  openNovoProduto(){
    this.navCtrl.push(CadastroProduto);
  }

  openVeganator() {
    this.navCtrl.push(Veganator);
  };

  openProdutosVego() {
    this.navCtrl.push(ListaProdutoVego);
  };

  openNovosProdutos(){
    this.navCtrl.push(ListaProdutoVegoNovos);
  }

  openProdutosEmAlta(){
    this.navCtrl.push(ListaProdutoVegoAlta);
  }


  doLogout() {
    let alert = this.alertCtrl.create({
      title: 'Tem certeza?',
      message: 'Você realmente tem certeza que deseja sair?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancelar',
          handler: () => {
            console.log('Logout Cancelado');
          }
        },
        {
          text: 'Sair',
          handler: () => {
            this.storage.clear();
            this.navCtrl.push(FirstRunPage);
          }
        }
      ]
    });
    alert.present();

  }
  
  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
    });
  }

  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }
}
