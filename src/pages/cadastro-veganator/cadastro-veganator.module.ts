import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CadastroVeganatorPage } from './cadastro-veganator';

@NgModule({
  declarations: [
    CadastroVeganatorPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroVeganatorPage),
    TranslateModule.forChild()
  ],
  exports: [
    CadastroVeganatorPage
  ]
})
export class CadastroVeganatorPageModule { }
