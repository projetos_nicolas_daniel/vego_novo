import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { Api } from '../../providers';
import { Storage } from '@ionic/storage';
import { Veganator } from '..';

@IonicPage()
@Component({
  selector: 'page-cadastro-veganator',
  templateUrl: 'cadastro-veganator.html'
})
export class CadastroVeganatorPage {

  userName: string;
  token: string;
  veganator: any;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public api: Api,
    public storage: Storage) {
    this.getUser();
    this.getToken();
    this.veganator = {
      empresa:'',
      ingrediente: '',
      produto: '',
      status: 'Pendente',
      usuario: '',
      whatsapp: ''
    };
  }


  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
    });
  }

  saveVeganator(){
    this.veganator.usuario = this.userName;
    this.veganator.ingrediente = this.userName;

    this.api.post("api/v1/veganators_cadastrados",this.veganator,{ headers: { 'x-access-token': this.token } }).subscribe(resp =>{
      alert("Veganator inserido com sucesso");     
      this.navCtrl.push(Veganator);
    },err=>{
      alert("Veganator inserido com sucesso");     
      this.navCtrl.push(Veganator);
    });
  }
}
