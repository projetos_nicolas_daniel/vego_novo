import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Items } from '../../providers';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})
export class ItemDetailPage {

  constructor(public navCtrl: NavController, navParams: NavParams, items: Items) {
    
  }

  redirect(){
    this.navCtrl.push(LoginPage);
  }

}
