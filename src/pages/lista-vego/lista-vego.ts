import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, NavParams, Menu, Content } from 'ionic-angular';
import { Api } from '../../providers';
import { Storage } from '@ionic/storage';
import { CadastroProduto } from '..';

@IonicPage()
@Component({
  selector: 'page-lista-vego',
  templateUrl: 'lista-vego.html'
})
export class ListaVegoPage {

  userName: string;
  token: string;
  produtos: any[];
  keys: any[];

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public api: Api,
    public storage: Storage, public navParams: NavParams) {
    this.produtos = [];
    this.produtos = this.navParams.get('lista');
    this.keys = this.navParams.get('keys');
    if (this.produtos == undefined)
      this.navCtrl.push('ContentPage');

    this.getUser();
    this.getToken();
  }

  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
    });
  }

  showDetailsProduct(product) {
    console.log(product)
    if (product['Nome Lista'] == 'Lista de Ingredientes' || product['Nome'].includes("Empresa")){
      alert('Não é permitido criar um produto a partir de um ingrediente');
    }else{
      this.navCtrl.push(CadastroProduto, { 
        produto: product._id, 
        nome: product['Nome'].includes("Empresa") ? '' : product.Nome, //dl comentou set 2020
        //nome: product['Nome'] ? '' : product.Nome, //dl inseriu aqui set 2020
        lista: product['Nome Lista'] });
    }
  }

}
