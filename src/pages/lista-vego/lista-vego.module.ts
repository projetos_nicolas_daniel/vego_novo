import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ListaVegoPage } from './lista-vego';

@NgModule({
  declarations: [
    ListaVegoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaVegoPage),
    TranslateModule.forChild()
  ],
  exports: [
    ListaVegoPage
  ]
})
export class ListaVegoPageModule { }
