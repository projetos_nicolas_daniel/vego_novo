import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { Api } from '../../providers';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-comida-caseira',
  templateUrl: 'comida-caseira.html'
})
export class ComidaCaseiraPage {
//ANDREI
  racas: any[];
  sexos: any[];
  valoresFiltros: any;
  filtros: any[];
  valoresTop: any[];
  amountWithPage: number;
  token: string;
  userName: string;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public api: Api,
    public storage: Storage) {

    this.sexos = [
      { valor: 1, nome: 'Macho' },
      { valor: 2, nome: 'Femea' },
      { valor: 3, nome: 'Ambos' }
    ];
    this.racas = [
      { valor: 1, 'nome': 'Nelore' },
      { valor: 2, 'nome': 'Guzerá' },
      { valor: 4, 'nome': 'Brahman' },
      { valor: 8, 'nome': 'Tabapuã' },
    ];
    this.valoresFiltros = {
      'topmgt': 100, 'amgt': 0.01,
      'topddpg': 100, 'adpg': 0.01,
      'topddipp': 100, 'adipp': 0.01,
      'topddpp30': 100, 'adpp30': 0.01,
      'topddstay': 100, 'adstay': 0.01,
      'topddpac': 100, 'adpac': 0.01,
      'topddpe365': 100, 'adpe365': 0.01,
      'topddpe455': 100, 'adpe455': 0.01,
      'topdmpp120': 100, 'ampp120': 0.01,
      'topdmpp240': 100, 'ampp240': 0.01,
      'topddpn': 100, 'adpn': 0.01,
      'topddpp120': 100, 'adpp120': 0.01,
      'topddpp240': 100, 'adpp240': 0.01,
      'topddpp365': 100, 'adpp365': 0.01,
      'topddpp455': 100, 'adpp455': 0.01,
      'topddpav': 100, 'adpav': 0.01,
      'topddaol': 100, 'adaol': 0.01,
      'topddp8': 100, 'adp8': 0.01,
      'topddmar': 100, 'admar': 0.01,
      'topddmac': 100, 'admac': 0.01,
      'topddpcq': 100, 'adpcq': 0.01,
      'topddppc': 100, 'adppc': 0.01,
      'topddcar': 100, 'adcar': 0.01,
      'topddims': 100, 'adims': 0.01,
      'raca': 1, 'sexo': 1,
      'topddpe450': 100, 'adpe450': 0.01,
      'topdmtp120': 100, 'amtp120': 0.01,
      'topdmtp240': 100, 'amtp240': 0.01,
      'topdded': 100, 'aded': 0.01,
      'topddpd': 100, 'adpd': 0.01,
      'topddmd': 100, 'admd': 0.01,
      'topddes': 100, 'ades': 0.01,
      'topddps': 100, 'adps': 0.01,
      'topddms': 100, 'adms': 0.01,
      'topddalt': 100, 'adalt': 0.01,
    }
    this.filtros = [
      { 'nome': 'MGTe', 'acuracia': 'amgt', 'top': 'topmgt', 'visivel': false },
      { 'nome': 'DPG', 'acuracia': 'adpg', 'top': 'topddpg', 'visivel': false },
      { 'nome': 'DIPP', 'acuracia': 'adipp', 'top': 'topddipp', 'visivel': false },
      { 'nome': 'D3P', 'acuracia': 'adpp30', 'top': 'topddpp30', 'visivel': false },
      { 'nome': 'DSTAY', 'acuracia': 'adstay', 'top': 'topddstay', 'visivel': false },
      { 'nome': 'DPAC', 'acuracia': 'adpac', 'top': 'topddpac', 'visivel': false },
      { 'nome': 'DPE365', 'acuracia': 'adpe365', 'top': 'topddpe365', 'visivel': false },
      { 'nome': 'DPE450', 'acuracia': 'adpe455', 'top': 'topddpe450', 'visivel': false },
      { 'nome': 'MP120', 'acuracia': 'ampp120', 'top': 'topdmpp120', 'visivel': false },
      { 'nome': 'MP210', 'acuracia': 'ampp240', 'top': 'topdmpp240', 'visivel': false },
      { 'nome': 'DPN', 'acuracia': 'adpn', 'top': 'topddpn', 'visivel': false },
      { 'nome': 'DP120', 'acuracia': 'adpp120', 'top': 'topddpp120', 'visivel': false },
      { 'nome': 'DP210', 'acuracia': 'adpp240', 'top': 'topddpp240', 'visivel': false },
      { 'nome': 'DP365', 'acuracia': 'adpp365', 'top': 'topddpp365', 'visivel': false },
      { 'nome': 'DP450', 'acuracia': 'adpe455', 'top': 'topddpp455', 'visivel': false },
      { 'nome': 'DPAV', 'acuracia': 'adpav', 'top': 'topddpav', 'visivel': false },
      { 'nome': 'DAOL', 'acuracia': 'adaol', 'top': 'topddaol', 'visivel': false },
      { 'nome': 'DACAB', 'acuracia': 'adp8', 'top': 'topddp8', 'visivel': false },
      { 'nome': 'DMAR', 'acuracia': 'admar', 'top': 'topddmar', 'visivel': false },
      { 'nome': 'DMAC', 'acuracia': 'admac', 'top': 'topddmac', 'visivel': false },
      { 'nome': 'DPCQ', 'acuracia': 'adpcq', 'top': 'topddpcq', 'visivel': false },
      { 'nome': 'DPPC', 'acuracia': 'adppc', 'top': 'topddppc', 'visivel': false },
      { 'nome': 'DCAR', 'acuracia': 'adcar', 'top': 'topddcar', 'visivel': false },
      { 'nome': 'DIMS', 'acuracia': 'adims', 'top': 'topddims', 'visivel': false }
    ]
    this.valoresTop = [
      { 'valor': 0.1, 'nome': '0.1%' },
      { 'valor': 0.5, 'nome': '0.5%' },
      { 'valor': 1, 'nome': '1%' },
      { 'valor': 2, 'nome': '2%' },
      { 'valor': 3, 'nome': '3%' },
      { 'valor': 4, 'nome': '4%' },
      { 'valor': 5, 'nome': '5%' },
      { 'valor': 10, 'nome': '10%' },
      { 'valor': 15, 'nome': '15%' },
      { 'valor': 20, 'nome': '20%' },
      { 'valor': 25, 'nome': '25%' },
      { 'valor': 30, 'nome': '30%' },
      { 'valor': 40, 'nome': '40%' },
      { 'valor': 50, 'nome': '50%' },
      { 'valor': 60, 'nome': '60%' },
      { 'valor': 70, 'nome': '70%' },
      { 'valor': 80, 'nome': '80%' },
      { 'valor': 90, 'nome': '90%' },
      { 'valor': 100, 'nome': '100%' }
    ];
    this.getUser();
    this.getToken();
    this.amountWithPage = 100;
  }

  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
    });
  }

  mostrarFiltro(index){
    this.filtros[index].visivel = !this.filtros[index].visivel;
  }
  
  searchFilter() {
    let loading = this.loadingCtrl.create({
      content: 'Buscando animais...'
    });
    loading.present();
    for(let attr in this.valoresFiltros) {
      if (attr.indexOf('a') == 0) {
        this.valoresFiltros[attr] = parseFloat(this.valoresFiltros[attr]);
      }
  }
    this.valoresFiltros['username'] = this.userName;
    this.valoresFiltros['quantidade'] = this.amountWithPage;
    this.api.post("api/v1/mgte", this.valoresFiltros, { headers: { 'x-access-token': this.token } }).subscribe((sucesso: any) => {
      loading.dismiss();
      var animais = sucesso.doc;
      // $rootScope.tipoRetorno = 'text';
      this.storage.set('animais', JSON.stringify(animais));
      this.navCtrl.push('ListaPage');
    }, function (erro) {
      loading.dismiss();
      let loadingErro = this.loadingCtrl.create({
        content: 'Buscando animais...'
      });
      loadingErro.present();
    });
  };

}
