import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ComidaCaseiraPage } from './comida-caseira';

@NgModule({
  declarations: [
    ComidaCaseiraPage,
  ],
  imports: [
    IonicPageModule.forChild(ComidaCaseiraPage),
    TranslateModule.forChild()
  ],
  exports: [
    ComidaCaseiraPage
  ]
})
export class ComidaCaseiraPageModule { }
