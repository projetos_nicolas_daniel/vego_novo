import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, LoadingController, NavParams } from 'ionic-angular';
import { Api } from '../../providers';
import { Storage } from '@ionic/storage';
import { ConsultaLista, MainPage } from '..';
import { HttpClient } from '@angular/common/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera, CameraOptions } from '@ionic-native/camera';
@IonicPage()
@Component({
  selector: 'page-cadastro-produto',
  templateUrl: 'cadastro-produto.html'
})
export class CadastroProdutoPage {

  searchInput: string;
  amountWithPage: number;
  userName: string;
  token: string;
  produto: any;
  categorias: string[];
  avaliacao: any;
  base64Image: string;
  avaliacoes: any[];
  id: string;
  idTerceiro: string;
  nome: string;
  lista: string;
  categoriasSeparadas: any;
  empresa: any;
  

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public api: Api, private barcodeScanner: BarcodeScanner,
    public storage: Storage, private camera: Camera, private http: HttpClient, public navParams: NavParams,  private cdr: ChangeDetectorRef) {
    this.getUser();
    this.getToken();
    this.amountWithPage = 100;

    this.produto = {
      codigoDeBarras: '',
      empresa: '',
      ingredientes: '',
      produto: '',
      status: 'Pendente',
      tipo: '',
      username: '',
      usuario: '',
      foto: '',
      avaliacoes: [],
      emp_des: '' //dl inseriu set 2020
    };

    this.id = this.navParams.get('produto');
    this.nome = this.navParams.get('nome');
    this.lista = this.navParams.get('lista');

    this.categoriasSeparadas = {
      vegano: 0,
      vegetariano: 0,
      naoVegetariano: 0
    }

    this.categorias = ['Não Vegano', 'Vegano', 'Vegetariano'];
    this.avaliacao = {
      comentario: '',
      categoria: ''
    }
    this.base64Image = 'assets/imgs/sem-foto.png';
    this.avaliacoes = [
      { categoria: 'Vegano', comentario: 'Comentario 01 sobre o produto tal porque estou fazendo um teste' },
      { categoria: 'Não Vegano', comentario: 'Comentario 02 sobre o produto tal porque estou fazendo um teste' },
      { categoria: 'Vegetariano', comentario: 'Comentario 03 sobre o produto tal porque estou fazendo um teste' },
      { categoria: 'Vegano', comentario: 'Comentario 04 sobre o produto tal porque estou fazendo um teste' },
    ]
  }

  getProduct() {
    this.api.get("api/v2/produto_vego/" + this.id, null, { headers: { 'x-access-token': this.token } }, false).subscribe((resp: any) => {
      this.produto = resp.produto[0];
      if (this.produto != undefined) {
       try {this.base64Image = this.produto.foto.replace('foto do produto: ', ''); this.separarAvaliacoes();} 
       catch {}
      } else {
        this.idTerceiro = '' + this.id;
        this.id = undefined;
        this.produto = {
          codigoDeBarras: '',
          empresa: '',
          ingredientes: '',
          produto: this.nome ? this.nome : '',
          status: 'Pendente',
          tipo: '',
          username: '',
          usuario: '',
          foto: '',
          avaliacoes: [],
          emp_des: this.empresa ? this.empresa : 'true' //dl inseriu set 2020
        };
      }
    })
  }

  sendBase64(base64, type, maxResults) {
    return {
      "requests": [
        {
          "image": {
            "content": base64 //DL comentou aqui
           //"content": base64Image // DL criou aqui
          },
          "features": [
            {
              "type": type,
              "maxResults": maxResults
            }
          ]
        }
      ]
    }
  }

  sendPhoto(photo) { //DL comentou aqui 8 agosto de 2020
    //sendPhoto(imageData){ //DL inseriu aqui em 8 de agosto de 2020
    let loading = this.loadingCtrl.create({
      content: 'Serviço verificando..'
    });
    loading.present();
    var key = "AIzaSyDym32E7bkzQkCEGLCCoOlKQtJdRxvdKms" //DL inseriu chave em 10 de agosto de 2020 conta raysildo.lobo@gmail.com
    var urlGoogle = 'https://vision.googleapis.com/v1/images:annotate?key=' + key; //NICOLAS
    var enviado = this.sendBase64(photo, 'TEXT_DETECTION', 1); //DL comentou aqui 8/8/2020
    return this.http.post(urlGoogle, enviado).subscribe(data => {
      // return this.api.postExterno(urlGoogle, enviado).subscribe(data => {
      loading.dismiss();
      if (data['responses'] != undefined) {
        if (data['responses'][0]['textAnnotations'] != undefined) {
          if (data['responses'][0]['textAnnotations'][0]['description'] != undefined) {
            this.produto.ingredientes = data['responses'][0]['textAnnotations'][0]['description'];
          }
          else {
            alert('Nenhum texto encontrado, tente novamente1.');
          }
        }
        else {
          alert('Nenhum texto encontrado, tente novamente2.');
        }
      }
      else {
        alert('Nenhum texto encontrado, tente novamente3.');
      }
    }, erro => {
      loading.dismiss();
      alert('Nenhum texto encontrado, tente novamente4.');
    });
  };

  sendPhotoProduto(photo) { //DL comentou aqui 8 agosto de 2020
    //sendPhoto(imageData){ //DL inseriu aqui em 8 de agosto de 2020
    let loading = this.loadingCtrl.create({
      content: 'Serviço verificando..'
    });
    loading.present();
    var key = "AIzaSyDym32E7bkzQkCEGLCCoOlKQtJdRxvdKms" //DL inseriu chave em 10 de agosto de 2020 conta raysildo.lobo@gmail.com
    var urlGoogle = 'https://vision.googleapis.com/v1/images:annotate?key=' + key; //NICOLAS
    var enviado = this.sendBase64(photo, 'TEXT_DETECTION', 1); //DL comentou aqui 8/8/2020
    return this.http.post(urlGoogle, enviado).subscribe(data => {
      // return this.api.postExterno(urlGoogle, enviado).subscribe(data => {
      loading.dismiss();
      if (data['responses'] != undefined) {
        if (data['responses'][0]['textAnnotations'] != undefined) {
          if (data['responses'][0]['textAnnotations'][0]['description'] != undefined) {
            this.produto.produto = data['responses'][0]['textAnnotations'][0]['description'];
          }
          else {
            alert('Nenhum texto encontrado, tente novamente1.');
          }
        }
        else {
          alert('Nenhum texto encontrado, tente novamente2.');
        }
      }
      else {
        alert('Nenhum texto encontrado, tente novamente3.');
      }
    }, erro => {
      loading.dismiss();
      alert('Nenhum texto encontrado, tente novamente4.');
    });
  };

  abrirIngrediente() {

    const options: CameraOptions = {
      quality: 90,
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      correctOrientation: true
    };

    this.camera.getPicture(options).then((imageData) => {
      this.sendPhoto(imageData);
    }, (err) => {
      alert('Ocorreu um erro ao processar foto');
    });
  }

  abrirProduto() {

    const options: CameraOptions = {
      quality: 90,
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      correctOrientation: true
    };

    this.camera.getPicture(options).then((imageData) => {
      this.sendPhotoProduto(imageData);
    }, (err) => {
      alert('Ocorreu um erro ao processar foto');
    });
  }

  tirarFotoProduto() {
    const options: CameraOptions = {
      quality: 90,
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      correctOrientation: true,
      targetWidth: 500,
      targetHeight: 500
    };

    this.camera.getPicture(options).then((imageData) => {
      debugger;
      console.log('foto do produto: ' + imageData);
      this.base64Image = "data:image/jpeg;base64," + imageData;
      //alert("Foto do Produto inserida com sucesso");
    }, (err) => {
      alert('Ocorreu um erro ao processar foto');
    });
  }

  avaliarProduto() {
    let loading = this.loadingCtrl.create({
      content: 'Avaliando Produto..'
    });
    loading.present();
    setTimeout(() => {
      this.produto.avaliacoes.push(this.avaliacao);
      loading.dismiss();
      alert("Avaliação Gerada com sucesso");
      this.saveProduct();
    }, 2000);

  }

  separarAvaliacoes() {
    this.produto.avaliacoes.forEach(avaliacao => {
      if (avaliacao.categoria == 'Não Vegano')
        this.categoriasSeparadas.naoVegetariano = this.categoriasSeparadas.naoVegetariano + 1;
      if (avaliacao.categoria == 'Vegetariano')
        this.categoriasSeparadas.vegetariano = this.categoriasSeparadas.vegetariano + 1;
      if (avaliacao.categoria == 'Vegano')
        this.categoriasSeparadas.vegano = this.categoriasSeparadas.vegano + 1;
    });
  }

  abrirCodigoBarras() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.produto.codigoDeBarras = barcodeData.text;
    }).catch(err => {
      console.log('Error', err);
    });
  }

  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
      if (this.id != undefined && this.id != "")
        this.getProduct();
    });
  }

  saveProduct() {
    this.produto.username = this.userName;
    this.produto.usuario = this.userName;
    this.produto.idTerceiro = this.idTerceiro ? this.idTerceiro : '';
    this.produto.listaTerceiro = this.lista ? this.lista : '';
    if (this.base64Image != "")
      this.produto.foto = this.base64Image;
      
    console.log(this.produto.foto);
    this.api.post("api/v2/cadastra_produto", this.produto, { headers: { 'x-access-token': this.token } }, false).subscribe(resp => {
      alert("Produto inserido com sucesso");
      //this.navCtrl.push(ConsultaLista); //DL comentou aqui em 11 de agosto de 2020 e criou o de baixo, estava indo para consulta e 
      this.navCtrl.push(MainPage); // e agora vai para a main page
    }, err => {
      alert("Produto nao foi inserido...;(");
      //this.navCtrl.push(ConsultaLista); //DL comentou aqui em 11 de agosto de 2020 e criou o de baixo, estava indo para consulta e 
      //this.navCtrl.push(MainPage); // e agora vai para a main page
    });
  }

  //isDisabled:boolean =true;  


}