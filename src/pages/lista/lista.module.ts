import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ListaPage } from './lista';

@NgModule({
  declarations: [
    ListaPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaPage),
    TranslateModule.forChild()
  ],
  exports: [
    ListaPage
  ]
})
export class ListaPageModule { }
