// The page the user lands on after opening the app and without a session
export const FirstRunPage = 'IndexPage';
export const LoginPage = 'LoginPage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'ContentPage';
export const ConsultaLista = 'ConsultaListaPage';
export const ComidaCaseira = 'ComidaCaseiraPage';
export const Veganator = 'VeganatorPage';
export const ReceitasSalvas = 'ReceitasSalvasPage';
export const ListaVego = 'ListaVegoPage';
export const CadastroProduto = 'CadastroProdutoPage';
export const CadastroLista = 'CadastroListaPage';
export const CadastroApoiadores = 'CadastroApoiadoresPage';
export const CadastroVeganator = 'CadastroVeganatorPage';
export const Lista = 'ListaPage';
export const ListaVeganator = 'ListaVeganatorPage';
export const ListaProdutoVego = 'ListaProdutoVegoPage';
export const ListaProdutoVegoAlta = 'ListaProdutoVegoAltaPage';
export const ListaProdutoVegoNovos = 'ListaProdutoVegoNovosPage';
export const Patrocinadores = 'PatrocinadoresPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'ListMasterPage';
export const Tab2Root = 'SearchPage';
export const Tab3Root = 'SettingsPage';
