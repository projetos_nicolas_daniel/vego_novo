import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers';

/**
 * Generated class for the PatrocinadoresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-patrocinadores',
  templateUrl: 'patrocinadores.html',
})
export class PatrocinadoresPage {

  apoiadores: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public api: Api) {
  }

  ionViewDidLoad() {
    this.obterPatrocinadores();
  }

  obterPatrocinadores(){
    //this.api.get('api/v2/apoiadores').subscribe((apoiadores: any) => {
      this.api.get('api/v2/apoiadores_cadastrados').subscribe((apoiadores: any) => {
      this.apoiadores = apoiadores.objeto;
    })
  }

}
