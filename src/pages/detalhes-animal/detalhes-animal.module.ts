import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { DetalhesAnimalPage } from './detalhes-animal';

@NgModule({
  declarations: [
    DetalhesAnimalPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesAnimalPage),
    TranslateModule.forChild()
  ],
  exports: [
    DetalhesAnimalPage
  ]
})
export class DetalhesAnimalPageModule { }
