import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Api } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-detalhes-animal',
  templateUrl: 'detalhes-animal.html'
})
export class DetalhesAnimalPage {

  racas: any[];
  sexos: any[];
  valoresFiltros: any;
  filtros: any[];
  valoresTop: any[];
  animal: any;
  touro: any;
  nomeDep: string;
  tblValorDep: string;
  cmpValorDep: string;
  tblAcuraciaDep: string;
  cmpAcuraciaDep: string;
  tblTopDep: string;
  cmpTopDep: string;
  infoAval: string;
  arvoreGenealogica: any;
  animalSalvo: boolean;
  userName: string;
  token: string;
  amount: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public loadingCtrl: LoadingController, public api: Api) {
    this.animal = this.navParams.get('animal');
    this.touro = this.navParams.get('touro');
    this.nomeDep = this.touro ? this.touro['nome'] : 'MGTe';
    this.tblValorDep = this.touro ? this.touro['tabelaValor'] : 'AnimalAGCriador';
    this.cmpValorDep = this.touro ? this.touro['campoValor'] : 'mgt';
    this.tblAcuraciaDep = this.touro ? this.touro['tabelaAcuracia'] : 'AnimalAGCriador';
    this.cmpAcuraciaDep = this.touro ? this.touro['campoAcuracia'] : 'amgt';
    this.tblTopDep = this.touro ? this.touro['tabelaTop'] : 'AnimalAGCriador4';
    this.cmpTopDep = this.touro ? this.touro['campoTop'] : 'mgt';
    this.amount = 10;
    this.getInfoAval();
    this.getToken();
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
      this.getUser();
    });
  }

  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
      this.verificarAnimalSalvo();
    });
  }

  getInfoAval() {
    this.storage.get('infoAval').then(resp => {
      var respInfoAval = JSON.parse(resp);
      this.infoAval = respInfoAval.infoaval;
    })
  }

  mostrarFiltro(index) {
    this.filtros[index].visivel = !this.filtros[index].visivel;
  }

  verificaSexoAnimal(sexo) {
    if (sexo == 1) {
      return 'Macho';
    } else if (sexo == 2) {
      return 'Fêmea';
    } else {
      return sexo;
    }
  };

  verificaDataNascimento(dtNasc) {
    if (dtNasc == '0000-00-00' || typeof dtNasc == "undefined") {
      return '-';
    } else {
      var data = dtNasc.split('-');
      return data[2] + '/' + data[1] + '/' + data[0];
    }
  }

  montarArvoreGenealogica(arvoreGenealogica) {
    this.arvoreGenealogica = arvoreGenealogica[0];

    arvoreGenealogica.shift();

    arvoreGenealogica.forEach(c => {
      if (this.arvoreGenealogica.pai_cga == c.cga) {
        this.arvoreGenealogica.pai = c;
      }

      if (this.arvoreGenealogica.mae_cga == c.cga) {
        this.arvoreGenealogica.mae = c;
      }
    });

    arvoreGenealogica.forEach(c => {
      if (this.arvoreGenealogica.pai.pai_cga == c.cga) {
        this.arvoreGenealogica.pai.pai = c;
      }

      if (this.arvoreGenealogica.pai.mae_cga == c.cga) {
        this.arvoreGenealogica.pai.mae = c;
      }

      if (this.arvoreGenealogica.mae.pai_cga == c.cga) {
        this.arvoreGenealogica.mae.pai = c;
      }

      if (this.arvoreGenealogica.mae.mae_cga == c.cga) {
        this.arvoreGenealogica.mae.mae = c;
      }
    });

    return this.arvoreGenealogica;
  };

  verificarAnimalSalvo() {

    this.storage.get('animaisSalvos').then(resp => {
      //Valida Animal Salvo
      var animaisSalvos = (resp.length > 0 ? JSON.parse(resp) : []);
      animaisSalvos.push(this.animal);
      animaisSalvos.forEach(element => {
        if (element.nome == this.animal.nome)
          this.animalSalvo = true;
      });

      var objeto = {
        username: this.userName,
        cgaanimal: this.animal.cga,
        quantidade: this.amount
      }

      this.api.post('api/v1/cga', objeto, { headers: { 'x-access-token': this.token } }).subscribe((resp: any) => {
        this.animal = resp.doc;
        this.api.post('api/v1/gen9a', objeto, { headers: { 'x-access-token': this.token } }).subscribe((ret: any) => {
          this.montarArvoreGenealogica(ret);
        })
      })
    });

  }

  saveBull() {
    this.storage.get('animaisSalvos').then(resp => {

      var animaisSalvos = (resp.length > 0 ? JSON.parse(resp) : []);

      animaisSalvos.push(this.animal);

      this.storage.set('animaisSalvos', JSON.stringify(animaisSalvos));

      this.animalSalvo = true;
    });
  }

  deleteBull() {
    this.storage.get('animaisSalvos').then(resp => {
      var listaAnimaisSalvos = [];
      var animaisSalvos = (resp.length > 0 ? JSON.parse(resp) : []);

      animaisSalvos.push(this.animal);

      animaisSalvos.forEach(element => {
        if (element.nome != this.animal.nome)
          listaAnimaisSalvos.push(element);
      });

      this.storage.set('animaisSalvos', JSON.stringify(listaAnimaisSalvos));

      this.animalSalvo = false;
    });
  }

  searchAnimal(animal) {
    this.navCtrl.push('DetalhesAnimalPage', { animal: animal, touro: this.touro });

  }

}
