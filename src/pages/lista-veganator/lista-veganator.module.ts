import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ListaVeganatorPage } from './lista-veganator';

@NgModule({
  declarations: [
    ListaVeganatorPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaVeganatorPage),
    TranslateModule.forChild()
  ],
  exports: [
    ListaVeganatorPage
  ]
})
export class ListaVeganatorPageModule { }
