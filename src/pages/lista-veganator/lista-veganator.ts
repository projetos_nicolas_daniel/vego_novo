import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, LoadingController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items, Api } from '../../providers';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-lista-veganator',
  templateUrl: 'lista-veganator.html'
})
export class ListaVeganatorPage {

  userName: string;
  token: string;
  veganatores: any[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public items: Items,
    public modalCtrl: ModalController,
    public api: Api,
    public storage: Storage,
    public loadingCtrl: LoadingController) {
      this.veganatores = [];
this.getUser();
    this.getToken();
  }


  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
      this.getLists();
    });
  }

  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getLists(){
    var objeto = {
      username:this.userName,
      animal_codigo:'',
      quantidade:100
    }
    this.api.post("api/v1/veganators_lista_produtos",objeto,{ headers: { 'x-access-token': this.token } }).subscribe((resp:any) => {
      this.veganatores = resp.err;
    })
  }

}
