import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, AlertController } from 'ionic-angular';
import { Api } from '../../providers';
import { Storage } from '@ionic/storage';
import { CadastroProduto, ListaVego, Lista, ListaProdutoVego } from '..';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@IonicPage()
@Component({
  selector: 'page-consulta-lista',
  templateUrl: 'consulta-lista.html'
})
export class ConsultaListaPage {

  listaTipoTouros: any[];
  keys: any[];
  searchView: boolean;
  searchInput: string;
  userName: string;
  token: string;
  codigoDeBarras: string;
  nomeConsulta: string;

  constructor(public navCtrl: NavController, public alertController: AlertController, public api: Api, public storage: Storage, private barcodeScanner: BarcodeScanner, public loadingCtrl: LoadingController) {
    this.nomeConsulta = '';
    this.getUser();
    this.getToken();
    this.keys = new Array();
  }

  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
    });
  }

  goCadastroProdutos() {
    this.navCtrl.push(CadastroProduto);
  };

  goListasVego() {
    this.navCtrl.push(ListaVego);
  }

  goProdutosVego() {
    this.navCtrl.push(ListaProdutoVego);
  }

  abrirCodigoBarras() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.codigoDeBarras = barcodeData.text;
    }).catch(err => {
      console.log('Error', err);
    });
  }

  goSearchLista() {
    let loading = this.loadingCtrl.create({
      content: 'Estamos buscando os produtos...'
    });
    loading.present();
    this.api.get("api/v2/codigo-de-barras/" + this.codigoDeBarras, null, { headers: { 'x-access-token': this.token } }).subscribe((resp: any) => {
      loading.dismiss();
      if (resp.status) {
        this.navCtrl.push(CadastroProduto, { produto: resp.objeto._id })//Descomentar isso para redirecionar para o produto!!
        //this.showModalProductNotFound(resp);
      } else {
        this.showModalProductNotFound("Não há produto com esse código de barras!");
      }
    })
  }

  async goSearchByName() {
    var originalText = this.nomeConsulta; //dl inseriu em set 2020
    var result = originalText.normalize('NFD').replace(/[\u0300-\u036f]/g, ""); //dl inseriu em set 2020
    console.log(originalText); //dl inseriu em set 2020
    console.log(result); //dl inseriu em set 2020
    //if (this.nomeConsulta.length < 3) {  //original
    if (result.length < 3) { //dl inseriu em set 2020
      const alert = await this.alertController.create({
        message: 'bb, tem q ter mais letras para eu conseguir procurar querida',
      });

    } else {
      var listaResultados = [];
      //this.api.get('api/v2/busca-produtos-todas-listas/' + this.nomeConsulta.toString(), null, { headers: { 'x-access-token': this.token } }).subscribe((resp: any) => { //original
      this.api.get('api/v2/busca-produtos-todas-listas/' + result.toString(), null, { headers: { 'x-access-token': this.token } }).subscribe((resp: any) => { //dl inseriu em set 2020
        if (resp.objeto != undefined) {
          resp.objeto.CompaniesDontTest.forEach(element => {
            //listaResultados.push(element); //dl comentei aqui em 20 set 2020
            listaResultados.push({
              '_id': element._id,
              'Nome': 'Empresa '+element.empresa,
              'Informacao 1': element.vegan,
              'Informacao 2': element.logo_peta,
              'Fonte': element.fonte,
              'Nome Lista': 'Lista PETA Brasil'
            });
            //this.keys.push('Nome','Nome Lista', 'Vegan', 'Logo PETA','Fonte');
            this.keys.push('Nome','Informacao 1', 'Informacao 2', 'Fonte','Nome Lista');
          });
          resp.objeto.Ingredientes.forEach(element => {
            listaResultados.push({
              '_id': element._id,
              'Nome': element.ingrediente,
              'Informacao 1': 'Esse ingrediente pode ser subtituido por '+element.alternativa,
              'Informacao 2': element.ingrediente_descricao,
              'Informacao 3': element.origem,
              'Fonte': element.fonte,
              //'Tipo': element.nome_lista, //dl inseriu em set 2020
              'Nome Lista': element.nome_lista
            });
            //this.keys.push('Nome', 'Nome Lista', 'Alternativa', 'Descrição', 'Tipo', 'Origem 2', 'Fonte');
            this.keys.push('Nome','Informacao 1', 'Informacao 2', 'Informacao 3', 'Fonte','Nome Lista');
          });
          resp.objeto.ListaTroll.forEach(element => {
            listaResultados.push({
              '_id': element._id,
              'Nome': element.item,
              'Informacao 1': element.opcao1, //DL apos o element, deve ter exatamente o que esta no documento do mongodb
              'Informacao 2': element.opcao2,
              'Informacao 3': element.opcao3,
              'Informacao 4': element.opcao4,
              //'Fonte': element.fonte,
              //'Nome Lista': 'Lista Troll' // DL inseriu versao
              'Nome Lista': element.versao // DL inseriu versao
            });
            //this.keys.push('Nome', 'Nome Lista', 'Opçāo 1', 'Opçāo 2', 'Opçāo 3', 'Opçāo 4','Nome Lista');
            this.keys.push('Nome','Informacao 1', 'Informacao 2', 'Informacao 3', 'Informacao 4', 'Fonte','Nome Lista');
          });
          resp.objeto.PetaBrasil.forEach(element => {
            listaResultados.push({
              '_id': element._id,
              'Nome': 'Empresa '+element.empresa,
              'Informacao 1': element.vegan,
              'Informacao 2': element.logo_peta,
              'Fonte': element.fonte,
              'Nome Lista': 'Lista PETA Brasil'
            });
            //this.keys.push('Nome','Nome Lista', 'Vegan', 'Logo PETA','Fonte');
            this.keys.push('Nome','Informacao 1', 'Informacao 2', 'Fonte','Nome Lista');
          });
          resp.objeto.PetaBrazil.forEach(element => {
            listaResultados.push({
              '_id': element._id,
              'Nome': 'Empresa '+element.empresa,
              'Informacao 1': element.vegan,
              'Informacao 2': element.logo_peta,
              'Fonte': element.fonte,
              'Nome Lista': 'Lista PETA Brasil'
            });
            //this.keys.push('Nome','Nome Lista', 'Vegan', 'Logo PETA','Fonte');
            this.keys.push('Nome','Informacao 1', 'Informacao 2', 'Fonte','Nome Lista');
          });
          resp.objeto.PetaEmpresasNacionais.forEach(element => {
            listaResultados.push({
              '_id':element._id,
              'Nome': 'Empresa '+element.empresa,
              'Informacao 1': element.marca,
              'Informacao 2': element.url,
              'Fonte': element.referencia,
              'Nome Lista': 'Peta Empresas Nacionais'
            });
            //this.keys.push('Nome','Nome Lista','Marca','URL','Referência');
            this.keys.push('Nome','Informacao 1', 'Informacao 2', 'Fonte','Nome Lista');
          });
          resp.objeto.ProdutosVego.forEach(element => {
            listaResultados.push({
              '_id': element._id,
              'Nome': element.produto,
              'Informacao 1': element.empresa,
              'Informacao 2':element.tipo,
              'Informacao 3':element.Ingredientes,
              'Fonte': element.fonte,
              'Nome Lista': 'EhVeg'
            });
            //this.keys.push('Nome','Nome Lista','Empresa','Tipo','Ingredientes');
            this.keys.push('Nome','Informacao 1', 'Informacao 2', 'Fonte','Nome Lista');
          });
          resp.objeto.ProdutosVego2.forEach(element => { //dl inseriu aqui set 2020
            listaResultados.push({
              '_id': element._id,
              'Nome': element.produto,
              'Informacao 1': element.empresa,
              'Informacao 2':element.tipo,
              'Informacao 3':element.Ingredientes,
              'Nome Lista': 'EhVeg'
            });
            //this.keys.push('Nome','Nome Lista','Empresa','Tipo','Ingredientes');
            this.keys.push('Nome','Informacao 1', 'Informacao 2', 'Fonte','Nome Lista');
          });
          if (listaResultados.length == 0) {
            this.showModalProductNotFound({ mensagem: 'Nenhum produto encontrado' });
          } else {
            this.keys = this.removeDups(this.keys);
            this.navCtrl.push(ListaVego, { lista: listaResultados, keys: this.keys });
          }
        } else {
          this.showModalProductNotFound({ mensagem: 'Nenhum produto encontrado' });
        }
      });
    }

  }
  removeDups(names) {
    let unique = {};
    names.forEach(function(i) {
      if(!unique[i]) {
        unique[i] = true;
      }
    });
    return Object.keys(unique);
  }

  async showModalProductNotFound(resposta) {
    const alert = await this.alertController.create({
      title: resposta.mensagem,
      message: 'O que deseja fazer agora?',
      buttons: [
        {
          text: 'Nova Busca',
          cssClass: 'secondary',
          handler: () => {
            this.codigoDeBarras = '';
          }
        }, {
          text: 'Cadastrar Produto',
          handler: () => {
            this.navCtrl.push(CadastroProduto);
          }
        }
      ]
    });

    await alert.present();
  }
}
