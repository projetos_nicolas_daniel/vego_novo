import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ConsultaListaPage } from './consulta-lista';

@NgModule({
  declarations: [
    ConsultaListaPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsultaListaPage),
    TranslateModule.forChild()
  ],
  exports: [
    ConsultaListaPage
  ]
})
export class ConsultaListaPageModule { }
