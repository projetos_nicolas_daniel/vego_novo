import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { VeganatorPage } from './veganator';

@NgModule({
  declarations: [
    VeganatorPage,
  ],
  imports: [
    IonicPageModule.forChild(VeganatorPage),
    TranslateModule.forChild()
  ],
  exports: [
    VeganatorPage
  ]
})
export class VeganatorPageModule { }
