import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CadastroVeganator, ListaVeganator } from '..';

@IonicPage()
@Component({
  selector: 'page-veganator',
  templateUrl: 'veganator.html'
})
export class VeganatorPage {

  searchInput: string;
  userName: string;
  token: string;

  constructor(public navCtrl: NavController,
    public storage: Storage,
    public loadingCtrl: LoadingController) {

    this.getUser();
    this.getToken();
    }


  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
    });
  }

  goCadastroLista(){
    this.navCtrl.push(CadastroVeganator);
  }

  goListaCadastrados(){
    this.navCtrl.push(ListaVeganator);
  }
}
