import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CadastroListaPage } from './cadastro-lista';

@NgModule({
  declarations: [
    CadastroListaPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroListaPage),
    TranslateModule.forChild()
  ],
  exports: [
    CadastroListaPage
  ]
})
export class CadastroProdutoPageModule { }
