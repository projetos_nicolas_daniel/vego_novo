import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { Api } from '../../providers';
import { Storage } from '@ionic/storage';
import { ConsultaLista, MainPage } from '..';

@IonicPage()
@Component({
  selector: 'page-cadastro-lista',
  templateUrl: 'cadastro-lista.html'
})
export class CadastroListaPage {

  userName: string;
  token: string;
  lista: any;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public api: Api,
    public storage: Storage) {
    this.getUser();
    this.getToken();
    this.lista = {
      empresa: '',
      ingrediente: '',
      whatsapp: '',
      produto:'',
      status:'Pendente',
      username: '',
      usuario:''
    };
  }


  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
    });
  }

  saveList(){
    this.lista.username = this.userName;
    this.lista.usuario = this.userName;

    this.api.post("api/v1/cadastra_lista",this.lista,{ headers: { 'x-access-token': this.token } }).subscribe(resp =>{
      alert("Lista inserida com sucesso");     
      this.navCtrl.push(MainPage);
    },err=>{
      alert("Lista inserida com sucesso");     
      this.navCtrl.push(MainPage);
    });
  }
}
