import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { Api } from '../../providers';
import { Storage } from '@ionic/storage';
import { CadastroProduto } from '..';

@IonicPage()
@Component({
  selector: 'page-lista-produto-vego',
  templateUrl: 'lista-produto-vego.html'
})
export class ListaProdutoVegoPage {

  userName: string;
  token: string;
  produtos: any[];

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public api: Api,
    public storage: Storage) {
    this.produtos = [];
      this.getUser();
    this.getToken();

  }

  getProducts(){
    
    this.api.get("api/v2/lista_produtos_vego",null,{ headers: { 'x-access-token': this.token } }, false).subscribe((resp:any) => {
      this.produtos = resp.lista;
      console.log(this.produtos);
    })
  }

  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
      this.getProducts();
    });
  }

  showDetailsProduct(product){
    console.log(product)
    this.navCtrl.push(CadastroProduto, { produto: product._id})
  }
  
}
