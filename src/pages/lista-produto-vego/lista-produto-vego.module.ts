import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaProdutoVegoPage } from './lista-produto-vego';


@NgModule({
  declarations: [
    ListaProdutoVegoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaProdutoVegoPage),
    TranslateModule.forChild()
  ],
  exports: [
    ListaProdutoVegoPage
  ]
})
export class ListaVegoPageModule { }
