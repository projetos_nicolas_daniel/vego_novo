import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, AlertController } from 'ionic-angular';
import { LoginPage, FirstRunPage } from '..'; //dl inseriu set 2020
import { Item } from '../../models/item';
import { Items, Api } from '../../providers';
import { Storage } from '@ionic/storage'; //DL inseriu em set 2020
import { MainPage } from '../';

@IonicPage()
@Component({
  selector: 'page-index',
  templateUrl: 'index.html'
})
export class IndexPage {

  produtos: any[] = [];
  constructor(public navCtrl: NavController, public api: Api, public alertCtrl: AlertController, public storage: Storage) { //dl inseriu public storage: Storage set 2020
    this.getProducts();
  }

  getProducts(){
    
    this.api.get("api/v2/produtos-listas",null,null, false).subscribe((resp:any) => {
      resp.objeto.ListaTroll.forEach(element => {
        this.produtos.push(element);
      });
      resp.objeto.ProdutosVego.forEach(element => {
        this.produtos.push(element);
      });
      console.log(this.produtos);
    })
  }

  cadastrarProduto(){
    const alert = this.alertCtrl.create({
      message: 'Para cadastrar novo produto EhVeg, você deve estar logado!',
      buttons:['OK']
    });
    alert.present();
    
    this.navCtrl.push('LoginPage');
    //this.navCtrl.push('ContentPage');
  }

  /*doLogin(){
    this.doLogout();
    this.navCtrl.push('LoginPage');
    //this.navCtrl.push('ContentPage');
  }*/


  account: { username: string, password: string } = {
    username: 'ecolobo@gmail.com',
    password: 'Rubi1501'
  };

  doLogin() {
      //loading.present();
      this.api.post("vegan-token3", this.account).subscribe((resp: any) => {
        this.storage.set('dados', resp);
        this.storage.set('logado', true);
        this.storage.set('token', resp.token);
        this.storage.set('cadastroAncp', (resp.user.cadastro_ancp == 1) ? true : false); //DL setei cadastro_ancp==0 era 1 antes
        this.storage.set('username', resp.user.username);
        this.storage.set('animaisSalvos', []);
        this.storage.set('infoAval', "");
          this.navCtrl.push(MainPage);

  })
}


  showDetailsProduct(item: Item) {
    const alert = this.alertCtrl.create({
      message: 'Para visualizar os detalhes, você deve estar logado!',
      buttons:['OK']
    });
    alert.present();
    
    this.navCtrl.push('LoginPage');
    //this.navCtrl.push('ContentPage');
  }
//dl inseriu abaixo em set 2020
doLogout() {
  this.storage.clear();
  //this.navCtrl.push(FirstRunPage);
  this.navCtrl.push(LoginPage); // dl LoginPage
}

}
