import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { Api } from '../../providers';
import { Storage } from '@ionic/storage';
import { CadastroApoiadores, MainPage } from '..';
import { Camera, CameraOptions } from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-cadastro-apoiadores',
  templateUrl: 'cadastro-apoiadores.html'
})
  export class CadastroApoiadoresPage {

  userName: string;
  token: string;
  lista: any;
  base64Image: string;
  

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public api: Api, private camera: Camera,
    public storage: Storage) {
    this.getUser();
    this.getToken();
    this.lista = {
      empresa: '',
      nome: '',
      ingrediente: '',
      whatsapp: '',
      produto:'',
      status:'Pendente',
      username: '',
      usuario:''
    };
    this.base64Image = 'assets/imgs/sem-foto.png';
  }


  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
    });
  }

  saveApoiadores(){
    this.lista.username = this.userName;
    this.lista.usuario = this.userName;
    if (this.base64Image != "")
    this.lista.imagem = this.base64Image;

      this.api.post("api/v1/apoiadores_cadastra",this.lista,{ headers: { 'x-access-token': this.token } }).subscribe(resp =>{
      alert("Apoiador inserido com sucesso");     
      this.navCtrl.push(MainPage);
    },err=>{
      alert("Apoiador inserido com sucesso");     
      this.navCtrl.push(MainPage);
    });
  }

  tirarFotoProduto() {
    const options: CameraOptions = {
      quality: 90,
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      correctOrientation: true,
      targetWidth: 500,
      targetHeight: 500
    };

    this.camera.getPicture(options).then((imageData) => {
      debugger;
      console.log('foto do produto: ' + imageData);
      this.base64Image = "data:image/jpeg;base64," + imageData;
      //alert("Foto do Produto inserida com sucesso");
    }, (err) => {
      alert('Ocorreu um erro ao processar foto');
    });
  }

}
