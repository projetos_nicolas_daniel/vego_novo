import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroApoiadoresPage } from './cadastro-apoiadores';

@NgModule({
  declarations: [
    CadastroApoiadoresPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroApoiadoresPage),
    TranslateModule.forChild()
  ],
  exports: [
    CadastroApoiadoresPage
  ]
})
export class CadastroApoiadoresPageModule { }