import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaProdutoVegoAltaPage } from './lista-produto-vego-alta';

@NgModule({
  declarations: [
    ListaProdutoVegoAltaPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaProdutoVegoAltaPage),
  ],
})
export class ListaProdutoVegoAltaPageModule {}
