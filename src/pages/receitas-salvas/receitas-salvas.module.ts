import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ReceitasSalvasPage } from './receitas-salvas';

@NgModule({
  declarations: [
    ReceitasSalvasPage,
  ],
  imports: [
    IonicPageModule.forChild(ReceitasSalvasPage),
    TranslateModule.forChild()
  ],
  exports: [
    ReceitasSalvasPage
  ]
})
export class ReceitasSalvasPageModule { }
