import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, LoadingController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items, Api } from '../../providers';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-receitas-salvas',
  templateUrl: 'receitas-salvas.html'
})
export class ReceitasSalvasPage {

  viewSearch: boolean;
  currentItems: Item[];
  farms: any[];
  bulls: any[];
  bullsSave: any;
  touro: any;
  token: string;
  userName: string;
  page: number;
  amount: number;
  amountWithPage: number;
  nomeDep: string;
  tblValorDep: string;
  cmpValorDep: string;
  tblAcuraciaDep: string;
  cmpAcuraciaDep: string;
  tblTopDep: string;
  cmpTopDep: string;
  carregando: boolean;
  loadingMore: boolean;
  loadingFarm: boolean;
  listaStatus: any[];
  sexos: any[];
  filtroPesquisa: any;
  fazenda: any;
  sexo: any;
  status: any;
  enableSearch: boolean
  searchInput: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public items: Items,
    public modalCtrl: ModalController,
    public api: Api,
    public storage: Storage,
    public loadingCtrl: LoadingController) {

    this.viewSearch = false;
    this.currentItems = this.items.query();
    this.page = 1;
    this.amount = 10;
    this.amountWithPage = 100;
    this.storage.get('animaisSalvos').then(resp => {
      this.bulls = resp.length > 0 ? JSON.parse(resp) : [];
      this.carregando = false;
      this.loadingMore = false;
    });
    this.nomeDep = 'MGTe';
    this.tblValorDep = 'AnimalAGCriador';
    this.cmpValorDep = 'mgt';
    this.tblAcuraciaDep = 'AnimalAGCriador';
    this.cmpAcuraciaDep = 'amgt';
    this.tblTopDep = 'AnimalAGCriador4';
    this.cmpTopDep = 'mgt';


    this.carregando = true;
    this.loadingFarm = true;

    this.sexos = [
      { valor: 1, nome: 'Macho' },
      { valor: 2, nome: 'Femea' },
      { valor: 3, nome: 'Ambos' }
    ];

    this.listaStatus = [
      { 'valor': '0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18', 'nome': 'Todos' },
      { 'valor': '0,1,3,5,6,8,9,10,11,12,13,14,15,16,17,18', 'nome': 'Ativo' },
      { 'valor': '2,4,7', 'nome': 'Inativo' }
    ];

  }

  searchList() {
    let loading = this.loadingCtrl.create({
      content: 'Filtrando animais...'
    });
    loading.present();
    if (!this.searchInput)
      this.storage.get('animaisSalvos').then(resp => {
        this.bulls = JSON.parse(resp);
        this.carregando = false;
        this.loadingMore = false;
      });
    else {
      let animais = Object.assign([],this.bulls);
      this.bulls = [];
      animais.forEach(filtro => {
        if (filtro.nome && (filtro.nome.toUpperCase().indexOf(this.searchInput.toUpperCase()) > -1)) {
          this.bulls.push(filtro);
        } else if (filtro.serie && (filtro.serie.toUpperCase().indexOf(this.searchInput.toUpperCase()) > -1)) {
          this.bulls.push(filtro);
        } else if (filtro.rgn && (filtro.rgn.toString().toUpperCase().indexOf(this.searchInput.toUpperCase()) > -1)) {
          this.bulls.push(filtro);
        } else if (filtro.rgd && (filtro.rgd.toString().toUpperCase().indexOf(this.searchInput.toUpperCase()) > -1)) {
          this.bulls.push(filtro);
        }
      });
      
    }
    loading.dismiss();
    this.viewSearch = false;
  }

  ionViewWillEnter() {
    this.getUser();
    this.getToken();
    this.clear();
  }

  openViewSearch() {
    this.viewSearch = !this.viewSearch;
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
    });
  }

  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  verificaDataNascimento(dtNasc) {
    if (dtNasc == '0000-00-00' || typeof dtNasc == "undefined") {
      return '-';
    } else {
      var data = dtNasc.split('-');
      return data[2] + '/' + data[1] + '/' + data[0];
    }
  }

  carregarMaisAnimais() {
    this.loadingMore = true;
    this.page = this.page + 1;
  }

  clear() {
    this.filtroPesquisa = {
      username: this.userName,
      pagina: 1,
      quantidade: this.amount,
      nfa: 0,
      sx: 0,
      usa: ''
    }
    this.status = '';
    this.sexo = '';
    this.fazenda = '';
  }

  viewDetails(animal) {
    this.navCtrl.push('DetalhesAnimalPage', { animal: animal });
  }
}
