import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaProdutoVegoNovosPage } from './lista-produto-vego-novos';

@NgModule({
  declarations: [
    ListaProdutoVegoNovosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaProdutoVegoNovosPage),
  ],
})
export class ListaProdutoVegoNovosPageModule {}
