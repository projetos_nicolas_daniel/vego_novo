import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { CadastroProduto } from '..';
import { Api } from '../../providers';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ListaProdutoVegoNovosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lista-produto-vego-novos',
  templateUrl: 'lista-produto-vego-novos.html',
})
export class ListaProdutoVegoNovosPage {
  userName: string;
  token: string;
  produtos: any[];

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public api: Api,
    public storage: Storage) {
    this.produtos = [];
      this.getUser();
    this.getToken();

  }

  ordenarPorAvaliacoes(a,b) {
    if (a.avaliacoes.length < b.avaliacoes.length)
      return -1;
    if (a.avaliacoes.length > b.avaliacoes.length)
      return 1;
    return 0;
  }

  getProducts(){
    this.api.get("api/v2/lista_produtos_avaliacoes",null,{ headers: { 'x-access-token': this.token } }, false).subscribe((resp:any) => {
        resp.lista = resp.lista.reverse();
        resp.lista = resp.lista.sort(this.ordenarPorAvaliacoes);
      if (resp.lista.length > 5){
        for (let index = 0; index < 5; index++) {
          this.produtos.push(resp.lista[index]);
        }
      }else{
        this.produtos = resp.lista;
      }
    })
  }

  getUser() {
    this.storage.get('username').then(resp => {
      this.userName = resp;
    });
  }

  getToken() {
    this.storage.get('token').then(token => {
      this.token = token;
      this.getProducts();
    });
  }

  showDetailsProduct(product){
    this.navCtrl.push(CadastroProduto, { produto: product._id})
  }
}
