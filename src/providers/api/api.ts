import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  //  url: string = 'http://testevegan.ecolog.com.br';
    // url: string = 'http://vegan.ecolog.com.br/';
    url: string = 'http://165.227.87.152:8090';
    // url: string = 'http://165.227.87.152:8091';
  // url: string = 'http://localhost:8090';

  constructor(public http: HttpClient) {
  }

  get(endpoint: string, params?: any, reqOpts?: any,urlCompleta?: boolean) {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }

    return urlCompleta ? this.http.get(endpoint, reqOpts) : this.http.get(this.url + '/' + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any, urlCompleta?: boolean) {
    return urlCompleta ? this.http.post(endpoint, body, reqOpts) : this.http.post(this.url + '/' + endpoint, body, reqOpts);
  }

  postExterno(url: string, body: any, reqOpts?: any) {
    return this.http.post(url, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
  }
}
