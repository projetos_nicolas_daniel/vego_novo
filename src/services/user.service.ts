import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { URL } from '../app/app.constants';

@Injectable()  
export class UserService {

  constructor(private http: HttpClient) {}


  save(newAccount): Observable<any> {
    return this.http.post<any>(URL.API + "signup", newAccount);
  }
}
