import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';

import { FirstRunPage } from '../pages';
import { Settings } from '../providers';

@Component({
  template: `<ion-menu [content]="content">
    <ion-header>
      <ion-toolbar>
        <ion-title>Menu</ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
          {{p.title}}
        </button>
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage = FirstRunPage;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Menu', component: 'ContentPage' },
    { title: 'Consulta Listas Produtos', component: 'ConsultaListaPage' },
    //{ title: 'Cadastro Apoiadores', component: 'CadastroApoiadoresPage' }, //DL inseriu em 22 set 2020
    { title: 'Novo Produto', component: 'CadastroProdutoPage' },
    { title: 'Nova Lista', component: 'CadastroListaPage' },
    { title: 'Lista de Produtos EhVeg', component: 'ListaProdutoVegoPage' }
  ]

  constructor(private translate: TranslateService, platform: Platform, 
    private statusBar: StatusBar, 
    private splashScreen: SplashScreen) {
      this.splashScreen.hide();
    
      platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.translate.setDefaultLang('pt-br');
    this.translate.use('pt-br');
    this.initTranslate();
    
  }

  initTranslate() {
    this.translate.setDefaultLang('pt-br');
    
    this.translate.use('pt-br');

    this.splashScreen.hide();
    //this.nav.push(FirstRunPage);
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}
